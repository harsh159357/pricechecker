# Price Checker

### [Assignment Apk](https://drive.google.com/file/d/1oeEwbd8uTulUrsyJWfdafk2-KFzJKPLb/view?usp=sharing)

### [Assignment in Action](https://drive.google.com/file/d/1lTitZ7ANucA-7x8Dd_TFn5BczLxDbDZM/view?usp=sharing)

### [Add Grocery to Cart Using QR Codes From Here](https://drive.google.com/drive/folders/1Nz0Q1pDxm-nJlBZIa5pxrzM8fYosWReg?usp=sharing)

## Overview of Assignment
* App Follows MVVM Architecture Design Pattern
* Dependency Injection implemented using Koin
* Room used for Local Database
* Images are loaded Lazily using Glide
* Swipe Left on Grocery Item to Delete from Cart.
* Ripple Effect on Item Touch
* Bitbucket and Git are used for VCS.
* 100 % Code in Kotlin


## Assignment By
# [Harsh Sharma](http://harsh159357.github.io/)

Senior Android Developer Infosys

[StackOverFlow](http://bit.ly/stackharsh)
[LinkedIn](http://bit.ly/lnkdharsh)
[Facebook](http://bit.ly/harshfb)
