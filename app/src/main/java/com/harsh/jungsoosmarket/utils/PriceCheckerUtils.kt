package com.harsh.jungsoosmarket.utils

import android.content.res.Resources
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.harsh.jungsoosmarket.models.CartItem
import com.harsh.jungsoosmarket.models.GroceryCartItem
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter
import java.io.Writer
import java.security.SecureRandom
import java.text.DecimalFormat
import java.util.*

class PriceCheckerUtils {
    companion object {
        fun getItemPrice(cartItem: GroceryCartItem): Double {
            return cartItem.cartItem.price.toDouble() * cartItem.quantity
        }

        fun formatToCurrency(amount: Double): String {
            val formatter = DecimalFormat("###,###,##0.00")
            return formatter.format(amount)
        }

        fun rawJsonToString(resources: Resources, id: Int): String {
            val resourceReader = resources.openRawResource(id)
            val writer: Writer = StringWriter()
            try {
                val reader = BufferedReader(InputStreamReader(resourceReader, "UTF-8"))
                var line = reader.readLine()
                while (line != null) {
                    writer.write(line)
                    line = reader.readLine()
                }
            } catch (e: Exception) {
                Log.e(
                    PriceCheckerUtils::class.simpleName,
                    "Unhandled exception while using rawJsonToString",
                    e
                )
            } finally {
                try {
                    resourceReader.close()
                } catch (e: Exception) {
                    Log.e(
                        PriceCheckerUtils::class.simpleName,
                        "Unhandled exception while using rawJsonToString",
                        e
                    )
                }
            }
            return writer.toString()
        }

        fun getGroceries(json: String): ArrayList<CartItem> {
            val productArrayList: ArrayList<CartItem>
            if (json != "") {
                productArrayList = Gson().fromJson<ArrayList<CartItem>>(
                    json,
                    object : TypeToken<ArrayList<CartItem?>?>() {}.type
                )
            } else {
                productArrayList = ArrayList<CartItem>()
            }
            return productArrayList
        }

        fun generateRandomInteger(min: Int, max: Int): Int {
            val rand = SecureRandom()
            rand.setSeed(Date().time)
            return rand.nextInt(max - min + 1) + min
        }
    }

}