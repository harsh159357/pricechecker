package com.harsh.jungsoosmarket.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.harsh.jungsoosmarket.models.CartAction
import com.harsh.jungsoosmarket.models.CartItem
import com.harsh.jungsoosmarket.models.GroceryCartItem
import com.harsh.jungsoosmarket.repository.PriceCheckerRepository
import com.harsh.jungsoosmarket.utils.PriceCheckerUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.ext.isInt

class PriceCheckerListViewModel(var repository: PriceCheckerRepository) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _cartAction = MutableLiveData<CartAction>()
    val cartCartAction: LiveData<CartAction>
        get() = _cartAction

    private val _total = MutableLiveData<String>()
    val total: LiveData<String>
        get() = _total

    private val _cart = MutableLiveData<List<GroceryCartItem>>()
    val groceryCart: LiveData<List<GroceryCartItem>>
        get() = _cart

    init {
        _cart.value = mutableListOf()
    }

    private lateinit var shoppingList: List<CartItem>
    private lateinit var lastRemovedCartItem: GroceryCartItem

    init {
        getShoppingList()
    }

    private fun getShoppingList() {
        _loading.value = true

        viewModelScope.launch(Dispatchers.IO) {
            shoppingList = repository.getShoppingList()
            withContext(Dispatchers.Main) {
                _loading.value = false
            }
        }
    }

    fun addToCart(id: String): Boolean {
        _loading.value = true

        try {
            val storeItem = if (id.isInt()) {
                shoppingList.find { item -> item.id == id }
            } else {
                val groceryNameArray = (id.split("-fresho-")[1]).replace(".jpg", "").split("-")
                val stringBuilder: StringBuilder = StringBuilder()
                for (item in groceryNameArray) {
                    stringBuilder.append(item.capitalize())
                    stringBuilder.append(" ")
                }
                shoppingList.find { item -> item.groceryName == stringBuilder.toString().trim() }
            }
            return if (storeItem != null) {
                addToCart(storeItem)
                true
            } else {
                _loading.value = false
                false
            }
        } catch (e: Exception) {
            _loading.value = false
            return false
        }
    }

    private fun addToCart(newCartItem: CartItem) {
        val cartList = _cart.value?.toMutableList() ?: mutableListOf()
        if (!cartList.isNullOrEmpty()) {
            var updated = false
            cartList.forEachIndexed { index, i ->
                i.takeIf { it.cartItem.id == newCartItem.id }?.let {
                    i.quantity++
                    updated = true
                }
            }
            if (!updated) {
                cartList.add(GroceryCartItem(1, newCartItem))
            }
        } else {
            cartList.add(GroceryCartItem(1, newCartItem))
        }

        //update live data values and recalculate total
        _loading.value = false
        _cart.value = cartList
        _cartAction.value = CartAction.ADD
        calculateTotal()
    }

    fun removeFromCart(index: Int) {
        _loading.value = true

        val cartList = _cart.value?.toMutableList() ?: mutableListOf()
        if (index < cartList.size) {
            lastRemovedCartItem = cartList[index]
            cartList.removeAt(index)

            _loading.value = false
            _cart.value = cartList
            _cartAction.value = CartAction.REMOVE
            calculateTotal()
        }
    }

    fun undoRemoveItem() {
        val cartList = _cart.value?.toMutableList() ?: mutableListOf()
        cartList.add(lastRemovedCartItem)
        _loading.value = false
        _cart.value = cartList
        calculateTotal()
    }

    private fun calculateTotal() {
        var total = 0.0
        groceryCart.value?.forEach {
            total += PriceCheckerUtils.getItemPrice(it)
        }

        _total.value = StringBuilder()
            .append("Total: $")
            .append(PriceCheckerUtils.formatToCurrency(total))
            .toString()
    }
}