package com.harsh.jungsoosmarket

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.integration.android.IntentIntegrator
import com.harsh.jungsoosmarket.adapters.GroceryListAdapter
import com.harsh.jungsoosmarket.adapters.SwipeToDeleteCallback
import com.harsh.jungsoosmarket.databinding.ActivityMainBinding
import com.harsh.jungsoosmarket.models.CartAction
import com.harsh.jungsoosmarket.utils.PriceCheckerUtils
import com.harsh.jungsoosmarket.viewModels.PriceCheckerListViewModel
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class MainActivity : AppCompatActivity() {
    private val shoppingListViewModel by inject<PriceCheckerListViewModel> { parametersOf(this) }
    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main).apply {
            lifecycleOwner = this@MainActivity
            viewModel = shoppingListViewModel
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.toolbar.title = getString(R.string.app_name)

        binding.toolbar.setOnMenuItemClickListener {
            shoppingListViewModel.addToCart(
                PriceCheckerUtils.generateRandomInteger(
                    0,
                    JungoosMarketApp.instance.getGroceryList().size
                ).toString()
            )
        }


        val listAdapter = GroceryListAdapter()
        binding.adapter = listAdapter

        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                shoppingListViewModel.removeFromCart(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(binding.shoppingList)

        shoppingListViewModel.groceryCart.observe(this, Observer {
            it.let {
                listAdapter.updateCart(it)
                if (it.isEmpty()) {
                    binding.shoppingList.visibility = View.GONE
                    binding.llNoItems.visibility = View.VISIBLE
                } else {
                    binding.shoppingList.visibility = View.VISIBLE
                    binding.llNoItems.visibility = View.GONE
                }
            }
        })

        shoppingListViewModel.cartCartAction.observe(this, Observer {
            when (it) {
                CartAction.ADD -> {
                    val sb =
                        Snackbar.make(binding.root, getString(R.string.item_added_to_cart), Snackbar.LENGTH_SHORT)
                    sb.anchorView = binding.fab
                    sb.show()
                }
                CartAction.REMOVE -> {
                    val snackBar =
                        Snackbar.make(binding.root, getString(R.string.item_removed_from_cart), Snackbar.LENGTH_LONG)
                    snackBar.setAction(getString(R.string.undo)) { shoppingListViewModel.undoRemoveItem() }
                    snackBar.anchorView = binding.fab
                    snackBar.show()
                }
                else -> {
                    //do nothing
                }
            }
        })
    }

    fun onClickScanQr(v: View) {
        val integrator = IntentIntegrator(this)

        integrator.setOrientationLocked(false)
        integrator.setPrompt(getString(R.string.scan_grocery_qr_code))
        integrator.setBeepEnabled(true)

        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)

        integrator.initiateScan()
    }

    //override method to get result from qr scanner
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, getString(R.string.qr_code_scan_cancelled), Toast.LENGTH_LONG).show()
            } else {
                if (!shoppingListViewModel.addToCart(result.contents)) {
                    Toast.makeText(this, getString(R.string.please_choose_valid_qr_code), Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}