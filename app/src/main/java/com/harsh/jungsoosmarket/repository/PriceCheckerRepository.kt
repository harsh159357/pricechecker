package com.harsh.jungsoosmarket.repository

import androidx.annotation.WorkerThread
import com.harsh.jungsoosmarket.models.CartItem
import com.harsh.jungsoosmarket.room.PriceCheckerDao

class PriceCheckerRepository(private val dao: PriceCheckerDao) {

    fun getShoppingList() = dao.getShoppingItems()

    @WorkerThread
    suspend fun insert(cartItem: CartItem) {
        dao.insert(cartItem)
    }

    @WorkerThread
    suspend fun remove(cartItem: CartItem) {
        dao.remove(cartItem)
    }

    @WorkerThread
    suspend fun clear() {
        dao.clear()
    }
}