package com.harsh.jungsoosmarket.models

data class GroceryCartItem(
    var quantity: Int,
    val cartItem: CartItem
)