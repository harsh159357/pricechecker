package com.harsh.jungsoosmarket.models

enum class CartAction {
    ADD,
    REMOVE
}