package com.harsh.jungsoosmarket.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "grocery_items")
open class CartItem (
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "groceryName") val groceryName: String,
    @ColumnInfo(name = "price") val price: String,
    @ColumnInfo(name = "groceryImgUrl") val groceryImgUrl: String,
)