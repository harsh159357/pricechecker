package com.harsh.jungsoosmarket.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.harsh.jungsoosmarket.databinding.GroceryCartItemBinding
import com.harsh.jungsoosmarket.models.GroceryCartItem

class GroceryListAdapter : RecyclerView.Adapter<GroceryListAdapter.ItemViewHolder>() {
    var groceryCartItems: MutableList<GroceryCartItem> = mutableListOf()

    class ItemViewHolder(val binding: GroceryCartItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun updateCart(cartItems: List<GroceryCartItem>) {
        groceryCartItems = cartItems.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = GroceryCartItemBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = groceryCartItems.get(position)
        holder.binding.groceryItem = currentItem
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return groceryCartItems.size
    }
}