package com.harsh.jungsoosmarket.room

import androidx.room.*
import com.harsh.jungsoosmarket.models.CartItem

@Dao
interface PriceCheckerDao {

    @Query("SELECT * FROM grocery_items ORDER BY id ASC")
    fun getShoppingItems(): List<CartItem>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(cartItem: CartItem)

    @Delete
    suspend fun remove(cartItem: CartItem)

    @Query("DELETE FROM grocery_items")
    suspend fun clear()
}