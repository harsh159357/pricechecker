package com.harsh.jungsoosmarket.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.Gson
import com.harsh.jungsoosmarket.JungoosMarketApp
import com.harsh.jungsoosmarket.models.CartItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Database(entities = [CartItem::class], version = 1, exportSchema = false)
abstract class PriceCheckerDatabase : RoomDatabase() {

    abstract fun priceCheckerDao(): PriceCheckerDao

    private class PriceCheckerDatabaseCallback(
        private val context: Context,
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            val gson = Gson()
            INSTANCE?.let { database ->
                scope.launch {
                    val priceCheckerDao = database.priceCheckerDao()

                    priceCheckerDao.clear()

                    JungoosMarketApp.instance.getGroceryList().forEach {
                        priceCheckerDao.insert(it)
                    }
                }
            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: PriceCheckerDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): PriceCheckerDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PriceCheckerDatabase::class.java,
                    "price_checker_database"
                )
                    .addCallback(PriceCheckerDatabaseCallback(context, scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}