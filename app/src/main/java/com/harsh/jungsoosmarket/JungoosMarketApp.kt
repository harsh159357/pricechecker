package com.harsh.jungsoosmarket

import android.app.Application
import com.harsh.jungsoosmarket.models.CartItem
import com.harsh.jungsoosmarket.utils.PriceCheckerUtils
import org.koin.android.ext.android.startKoin
import java.util.*

class JungoosMarketApp : Application() {

    companion object {
        lateinit var instance: JungoosMarketApp
            private set
    }

    private lateinit var groceries: ArrayList<CartItem>

    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin(this, listOf(viewModelModule, repositoryModule))
        initGroceries()
    }

    private fun initGroceries() {
        groceries = PriceCheckerUtils
            .getGroceries(
                PriceCheckerUtils
                    .rawJsonToString(
                        resources,
                        R.raw.groceries
                    )
            )
    }

    fun getGroceryList(): ArrayList<CartItem> {
        return groceries
    }
}