package com.harsh.jungsoosmarket

import com.harsh.jungsoosmarket.repository.PriceCheckerRepository
import com.harsh.jungsoosmarket.room.PriceCheckerDatabase
import com.harsh.jungsoosmarket.viewModels.PriceCheckerListViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

val viewModelModule = module {
    factory { PriceCheckerListViewModel(get()) }
}

val repositoryModule = module {
    single {
        PriceCheckerDatabase.getDatabase(androidApplication(),
            CoroutineScope(SupervisorJob())
        )
    }

    factory { PriceCheckerRepository(get<PriceCheckerDatabase>().priceCheckerDao()) }
}